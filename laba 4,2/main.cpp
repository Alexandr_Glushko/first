#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char** argv) {
	double x=0.5, sum=0;
	while (x<=4.8){
		sum=sum+((x+2*(pow(x, 3)))-sqrt(x));
		x+=0.2;
	}
	cout<<sum;
	return 0;
}
