#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char** argv) {
	double sum=0;
	for (double x=0.5; x<=4.8; x+=0.2){
		sum=sum+sin(2*(x+4*pow(x, 3)));
	}
	cout<<sum;
	return 0;
}
