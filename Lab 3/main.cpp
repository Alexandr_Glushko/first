#include<iostream>
#include<math.h>
 
using namespace std;
double f1(double z) 
{
	return sin(z)+tan(z);
}
double f2(double z)
{
	return pow(cos(z), 2) + 3/z;	
}
double f3(double z)
{
	return z*2 + log(pow(z, 2));	
}
double f(double z)
{
if (z<0)
	return f1(z);
if (z>=0 && z<=8)
	return f2(z);
if (z>8)
	return f3(z);
}
double ex(double z){
    return pow(f(z),4)+2*sin(pow(f(z),2));
}
 
int main(){
    cin>>z;
    cout<<f(z);
}
